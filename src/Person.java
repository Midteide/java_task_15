public class Person {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static String [][] colours = {{ANSI_BLACK, ANSI_RED, ANSI_GREEN, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN,ANSI_WHITE}, {ANSI_BLACK_BACKGROUND, ANSI_RED_BACKGROUND, ANSI_GREEN_BACKGROUND, ANSI_YELLOW_BACKGROUND, ANSI_BLUE_BACKGROUND, ANSI_PURPLE_BACKGROUND, ANSI_CYAN_BACKGROUND,ANSI_WHITE_BACKGROUND}};

    String fname;
    String lname;
    int mobNo;

    public Person (String firstname, String lastname, int mobilenumber) {
        this.fname = firstname;
        this.lname = lastname;
        this.mobNo = mobilenumber;
    }
    public Person () {
        this.fname = "";
        this.lname = "";
        this.mobNo = 0;
    }

    public boolean Search(String str) {
        boolean noMatch = true;

        if  ((this.lname.contains(str)) && (this.fname.contains(str))) {
            System.out.println("Match for \"" + ANSI_YELLOW + str + ANSI_RESET + "\" in both " + ANSI_CYAN +  "firstname and lastname" + ANSI_RESET + ": " + ANSI_GREEN + this.fname +  " " + ANSI_GREEN + this.lname + ANSI_RESET + " (Mob. No.: " + this.mobNo + ")" );
            noMatch=false;
        }
        else if  (this.lname.contains(str)) {
            System.out.println("Match for \"" + ANSI_YELLOW + str + ANSI_RESET + "\" in " + ANSI_CYAN +  "lastname" + ANSI_RESET + ": " + this.fname + " " + ANSI_GREEN + this.lname + ANSI_RESET  + " (Mob. No.: " + this.mobNo + ")" );
            noMatch=false;
        }
        else if (this.fname.contains(str)) {
            System.out.println("Match for \"" + ANSI_YELLOW + str + ANSI_RESET + "\" in " + ANSI_PURPLE +  "firstname" + ANSI_RESET + ": " + ANSI_GREEN + this.fname + ANSI_RESET + " " + this.lname + ANSI_RESET  + " (Mob. No.: " + this.mobNo + ")" );
            noMatch=false;
        }
        return noMatch;
    }


}